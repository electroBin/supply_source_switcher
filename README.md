This project includes a schematic as well as the Kicad project files for building a circuit to switch between two DC power sources. 
When the voltage of one of the sources (labeled V_LA in the schematic) drops to 10.8V, it is switched off and power is drawn from 
the source labeled V_Lith. The specific application in this case is for automotive use with V_LA being the 12V lead-acid car battery 
and V_Lith being an 18/20V lithium ion power tool battery (acting as a backup). This circuit can be be modified to switch between 
different voltage levels from different sources by modifying the resistor values on the comparator inputs. Hysterisis sensitivity 
can also be adjusted by altering the feedback resistor value of R9.