EESchema Schematic File Version 4
LIBS:SupplySourceSwitcher-cache
EELAYER 26 0
EELAYER END
$Descr B 17000 11000
encoding utf-8
Sheet 1 1
Title "Voltage Source Switching Circuit"
Date "2022-04-16"
Rev "1.0"
Comp "https://bitdrivencircuits.com/"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x02_Male J1
U 1 1 625B207F
P 6850 1800
F 0 "J1" V 6910 1841 50  0000 L CNN
F 1 "5V_supply" V 7001 1841 50  0000 L CNN
F 2 "Connector_JST:JST_XH_B02B-XH-A_1x02_P2.50mm_Vertical" H 6850 1800 50  0001 C CNN
F 3 "~" H 6850 1800 50  0001 C CNN
	1    6850 1800
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR0101
U 1 1 625B2213
P 6600 2250
F 0 "#PWR0101" H 6600 2100 50  0001 C CNN
F 1 "+5V" V 6615 2378 50  0000 L CNN
F 2 "" H 6600 2250 50  0001 C CNN
F 3 "" H 6600 2250 50  0001 C CNN
	1    6600 2250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6750 2000 6750 2100
Wire Wire Line
	6750 2250 6600 2250
$Comp
L power:GND #PWR0102
U 1 1 625B22B5
P 6850 2250
F 0 "#PWR0102" H 6850 2000 50  0001 C CNN
F 1 "GND" H 6855 2077 50  0000 C CNN
F 2 "" H 6850 2250 50  0001 C CNN
F 3 "" H 6850 2250 50  0001 C CNN
	1    6850 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 625B40EB
P 12800 4700
F 0 "D1" H 12792 4445 50  0000 C CNN
F 1 "LED_Yellow" H 12792 4536 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 12800 4700 50  0001 C CNN
F 3 "~" H 12800 4700 50  0001 C CNN
	1    12800 4700
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D2
U 1 1 625B413B
P 12750 6950
F 0 "D2" H 12742 6695 50  0000 C CNN
F 1 "LED_Blue" H 12742 6786 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 12750 6950 50  0001 C CNN
F 3 "~" H 12750 6950 50  0001 C CNN
	1    12750 6950
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R3
U 1 1 625B416C
P 12800 4200
F 0 "R3" V 13007 4200 50  0000 C CNN
F 1 "1.8kOhm" V 12916 4200 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 12730 4200 50  0001 C CNN
F 3 "~" H 12800 4200 50  0001 C CNN
	1    12800 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 625B41B6
P 12750 6450
F 0 "R2" V 12957 6450 50  0000 C CNN
F 1 "1.8kOhm" V 12866 6450 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 12680 6450 50  0001 C CNN
F 3 "~" H 12750 6450 50  0001 C CNN
	1    12750 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	12800 4550 12800 4350
Wire Wire Line
	12750 6800 12750 6600
$Comp
L power:GND #PWR0106
U 1 1 625B508B
P 12800 4850
F 0 "#PWR0106" H 12800 4600 50  0001 C CNN
F 1 "GND" V 12805 4722 50  0000 R CNN
F 2 "" H 12800 4850 50  0001 C CNN
F 3 "" H 12800 4850 50  0001 C CNN
	1    12800 4850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 625B50CC
P 12750 7100
F 0 "#PWR0107" H 12750 6850 50  0001 C CNN
F 1 "GND" V 12755 6972 50  0000 R CNN
F 2 "" H 12750 7100 50  0001 C CNN
F 3 "" H 12750 7100 50  0001 C CNN
	1    12750 7100
	1    0    0    -1  
$EndComp
$Comp
L Driver_FET:MIC4427 U2
U 1 1 625B8512
P 7950 5400
F 0 "U2" H 8050 5850 50  0000 C CNN
F 1 "MIC4427" H 8150 5750 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 7950 5100 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/mic4426.pdf" H 7950 5100 50  0001 C CNN
	1    7950 5400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 625B86F6
P 7950 5800
F 0 "#PWR05" H 7950 5550 50  0001 C CNN
F 1 "GND" H 7955 5627 50  0000 C CNN
F 2 "" H 7950 5800 50  0001 C CNN
F 3 "" H 7950 5800 50  0001 C CNN
	1    7950 5800
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR04
U 1 1 625B8783
P 7950 4750
F 0 "#PWR04" H 7950 4600 50  0001 C CNN
F 1 "+5V" H 7965 4923 50  0000 C CNN
F 2 "" H 7950 4750 50  0001 C CNN
F 3 "" H 7950 4750 50  0001 C CNN
	1    7950 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 5000 7950 4850
$Comp
L Device:C C2
U 1 1 625B8E46
P 8450 4850
F 0 "C2" V 8702 4850 50  0000 C CNN
F 1 "0.1uF" V 8611 4850 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 8488 4700 50  0001 C CNN
F 3 "~" H 8450 4850 50  0001 C CNN
	1    8450 4850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8300 4850 7950 4850
Connection ~ 7950 4850
Wire Wire Line
	7950 4850 7950 4750
$Comp
L power:GND #PWR06
U 1 1 625B9204
P 8600 4850
F 0 "#PWR06" H 8600 4600 50  0001 C CNN
F 1 "GND" V 8605 4722 50  0000 R CNN
F 2 "" H 8600 4850 50  0001 C CNN
F 3 "" H 8600 4850 50  0001 C CNN
	1    8600 4850
	0    -1   -1   0   
$EndComp
$Comp
L dk_Transistors-FETs-MOSFETs-Single:FQP27P06 Q1
U 1 1 625BDE48
P 10900 4250
F 0 "Q1" H 11007 4303 60  0000 L CNN
F 1 "Lithium_MOSFET" H 11007 4197 60  0000 L CNN
F 2 "digikey-footprints:TO-220-3" H 11100 4450 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/FQP27P06-D.PDF" H 11100 4550 60  0001 L CNN
F 4 "FQP27P06-ND" H 11100 4650 60  0001 L CNN "Digi-Key_PN"
F 5 "FQP27P06" H 11100 4750 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 11100 4850 60  0001 L CNN "Category"
F 7 "Transistors - FETs, MOSFETs - Single" H 11100 4950 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/FQP27P06-D.PDF" H 11100 5050 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/FQP27P06/FQP27P06-ND/965349" H 11100 5150 60  0001 L CNN "DK_Detail_Page"
F 10 "MOSFET P-CH 60V 27A TO-220" H 11100 5250 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 11100 5350 60  0001 L CNN "Manufacturer"
F 12 "Active" H 11100 5450 60  0001 L CNN "Status"
	1    10900 4250
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J3
U 1 1 625BF521
P 4000 1800
F 0 "J3" V 4060 1840 50  0000 L CNN
F 1 "Lithium_Batt_Supply" V 4151 1840 50  0000 L CNN
F 2 "Connector_JST:JST_XH_B02B-XH-A_1x02_P2.50mm_Vertical" H 4000 1800 50  0001 C CNN
F 3 "~" H 4000 1800 50  0001 C CNN
	1    4000 1800
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 625BF623
P 4000 2000
F 0 "#PWR0110" H 4000 1750 50  0001 C CNN
F 1 "GND" H 4005 1827 50  0000 C CNN
F 2 "" H 4000 2000 50  0001 C CNN
F 3 "" H 4000 2000 50  0001 C CNN
	1    4000 2000
	1    0    0    -1  
$EndComp
Text Label 3750 2150 2    50   ~ 0
V_Lith
Wire Wire Line
	3900 2000 3900 2150
Wire Wire Line
	3900 2150 3750 2150
Wire Wire Line
	10900 4450 10900 4650
Wire Wire Line
	10900 4650 11000 4650
$Comp
L Device:R R6
U 1 1 625C0A13
P 10400 4750
F 0 "R6" H 10470 4796 50  0000 L CNN
F 1 "10kOhm" H 10470 4705 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 10330 4750 50  0001 C CNN
F 3 "~" H 10400 4750 50  0001 C CNN
	1    10400 4750
	1    0    0    -1  
$EndComp
$Comp
L Device:D D3
U 1 1 625C163B
P 10900 3750
F 0 "D3" V 10854 3829 50  0000 L CNN
F 1 "1N4001" V 10945 3829 50  0000 L CNN
F 2 "Diode_THT:D_A-405_P7.62mm_Horizontal" H 10900 3750 50  0001 C CNN
F 3 "~" H 10900 3750 50  0001 C CNN
	1    10900 3750
	0    1    1    0   
$EndComp
Wire Wire Line
	10400 4600 10400 4350
Connection ~ 10400 4350
Wire Wire Line
	10400 4350 10600 4350
Wire Wire Line
	10400 4900 10400 5100
Wire Wire Line
	10400 5100 10500 5100
$Comp
L Connector:Conn_01x02_Male J4
U 1 1 625C8243
P 2550 1800
F 0 "J4" V 2610 1840 50  0000 L CNN
F 1 "LA_Batt_Supply" V 2701 1840 50  0000 L CNN
F 2 "Connector_JST:JST_XH_B02B-XH-A_1x02_P2.50mm_Vertical" H 2550 1800 50  0001 C CNN
F 3 "~" H 2550 1800 50  0001 C CNN
	1    2550 1800
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR07
U 1 1 625C82D7
P 2550 2000
F 0 "#PWR07" H 2550 1750 50  0001 C CNN
F 1 "GND" H 2555 1827 50  0000 C CNN
F 2 "" H 2550 2000 50  0001 C CNN
F 3 "" H 2550 2000 50  0001 C CNN
	1    2550 2000
	1    0    0    -1  
$EndComp
Text Label 11000 4650 0    50   ~ 0
V_Lith
Text Label 10500 5100 0    50   ~ 0
V_Lith
Text Label 2250 2150 2    50   ~ 0
V_LA
Wire Wire Line
	2250 2150 2450 2150
Wire Wire Line
	2450 2150 2450 2000
$Comp
L dk_Transistors-FETs-MOSFETs-Single:FQP27P06 Q2
U 1 1 625CACB0
P 10950 6400
F 0 "Q2" H 11057 6453 60  0000 L CNN
F 1 "LA_MOSFET" H 11057 6347 60  0000 L CNN
F 2 "digikey-footprints:TO-220-3" H 11150 6600 60  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/FQP27P06-D.PDF" H 11150 6700 60  0001 L CNN
F 4 "FQP27P06-ND" H 11150 6800 60  0001 L CNN "Digi-Key_PN"
F 5 "FQP27P06" H 11150 6900 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 11150 7000 60  0001 L CNN "Category"
F 7 "Transistors - FETs, MOSFETs - Single" H 11150 7100 60  0001 L CNN "Family"
F 8 "https://www.onsemi.com/pub/Collateral/FQP27P06-D.PDF" H 11150 7200 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/on-semiconductor/FQP27P06/FQP27P06-ND/965349" H 11150 7300 60  0001 L CNN "DK_Detail_Page"
F 10 "MOSFET P-CH 60V 27A TO-220" H 11150 7400 60  0001 L CNN "Description"
F 11 "ON Semiconductor" H 11150 7500 60  0001 L CNN "Manufacturer"
F 12 "Active" H 11150 7600 60  0001 L CNN "Status"
	1    10950 6400
	1    0    0    -1  
$EndComp
$Comp
L Device:D D4
U 1 1 625CB14F
P 10950 5800
F 0 "D4" V 10904 5879 50  0000 L CNN
F 1 "1N4001" V 10995 5879 50  0000 L CNN
F 2 "Diode_THT:D_A-405_P7.62mm_Horizontal" H 10950 5800 50  0001 C CNN
F 3 "~" H 10950 5800 50  0001 C CNN
	1    10950 5800
	0    1    1    0   
$EndComp
Text Label 11150 6800 0    50   ~ 0
V_LA
Wire Wire Line
	10950 6600 10950 6800
Wire Wire Line
	10950 6800 11150 6800
Text Label 10150 6500 2    50   ~ 0
LA_Mosfet_sig
Wire Wire Line
	10650 6500 10450 6500
$Comp
L Device:R R7
U 1 1 625CE44B
P 10450 6950
F 0 "R7" H 10520 6996 50  0000 L CNN
F 1 "10kOhm" H 10520 6905 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 10380 6950 50  0001 C CNN
F 3 "~" H 10450 6950 50  0001 C CNN
	1    10450 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	10450 6800 10450 6500
Text Label 10600 7350 0    50   ~ 0
V_Lith
Wire Wire Line
	10450 7100 10450 7350
Wire Wire Line
	10450 7350 10600 7350
$Comp
L Connector:Conn_01x02_Male J5
U 1 1 625D217E
P 15200 5800
F 0 "J5" V 15260 5840 50  0000 L CNN
F 1 "Power_Out" V 15351 5840 50  0000 L CNN
F 2 "Connector_JST:JST_XH_B02B-XH-A_1x02_P2.50mm_Vertical" H 15200 5800 50  0001 C CNN
F 3 "~" H 15200 5800 50  0001 C CNN
	1    15200 5800
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 625D340F
P 15000 5800
F 0 "#PWR0111" H 15000 5550 50  0001 C CNN
F 1 "GND" H 15005 5627 50  0000 C CNN
F 2 "" H 15000 5800 50  0001 C CNN
F 3 "" H 15000 5800 50  0001 C CNN
	1    15000 5800
	0    1    1    0   
$EndComp
$Comp
L dk_Linear-Comparators:LM339ADR U3
U 1 1 625DBB16
P 2750 6100
F 0 "U3" H 2950 6300 60  0000 C CNN
F 1 "LM339ANSR" H 3100 6200 60  0000 C CNN
F 2 "digikey-footprints:SOIC-14_W3.9mm" H 2950 6300 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Flm139a" H 2950 6400 60  0001 L CNN
F 4 "296-6604-1-ND" H 2950 6500 60  0001 L CNN "Digi-Key_PN"
F 5 "LM339ADR" H 2950 6600 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 2950 6700 60  0001 L CNN "Category"
F 7 "Linear - Comparators" H 2950 6800 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Flm139a" H 2950 6900 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/LM339ADR/296-6604-1-ND/404682" H 2950 7000 60  0001 L CNN "DK_Detail_Page"
F 10 "IC QUAD DIFF COMPARATOR 14 -SOIC" H 2950 7100 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 2950 7200 60  0001 L CNN "Manufacturer"
F 12 "Active" H 2950 7300 60  0001 L CNN "Status"
	1    2750 6100
	1    0    0    -1  
$EndComp
$Comp
L dk_Linear-Comparators:LM339ADR U3
U 3 1 625DD09F
P 2750 7150
F 0 "U3" H 2950 7350 60  0000 L CNN
F 1 "LM339ANSR" H 2950 7250 60  0001 L CNN
F 2 "digikey-footprints:SOIC-14_W3.9mm" H 2950 7350 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Flm139a" H 2950 7450 60  0001 L CNN
F 4 "296-6604-1-ND" H 2950 7550 60  0001 L CNN "Digi-Key_PN"
F 5 "LM339ADR" H 2950 7650 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 2950 7750 60  0001 L CNN "Category"
F 7 "Linear - Comparators" H 2950 7850 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Flm139a" H 2950 7950 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/LM339ADR/296-6604-1-ND/404682" H 2950 8050 60  0001 L CNN "DK_Detail_Page"
F 10 "IC QUAD DIFF COMPARATOR 14 -SOIC" H 2950 8150 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 2950 8250 60  0001 L CNN "Manufacturer"
F 12 "Active" H 2950 8350 60  0001 L CNN "Status"
	3    2750 7150
	1    0    0    -1  
$EndComp
$Comp
L dk_Linear-Comparators:LM339ADR U3
U 4 1 625DD105
P 2750 7700
F 0 "U3" H 2950 7900 60  0000 L CNN
F 1 "LM339ANSR" H 2950 7800 60  0001 L CNN
F 2 "digikey-footprints:SOIC-14_W3.9mm" H 2950 7900 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Flm139a" H 2950 8000 60  0001 L CNN
F 4 "296-6604-1-ND" H 2950 8100 60  0001 L CNN "Digi-Key_PN"
F 5 "LM339ADR" H 2950 8200 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 2950 8300 60  0001 L CNN "Category"
F 7 "Linear - Comparators" H 2950 8400 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Flm139a" H 2950 8500 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/LM339ADR/296-6604-1-ND/404682" H 2950 8600 60  0001 L CNN "DK_Detail_Page"
F 10 "IC QUAD DIFF COMPARATOR 14 -SOIC" H 2950 8700 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 2950 8800 60  0001 L CNN "Manufacturer"
F 12 "Active" H 2950 8900 60  0001 L CNN "Status"
	4    2750 7700
	1    0    0    -1  
$EndComp
$Comp
L dk_Linear-Comparators:LM339ADR U3
U 2 1 625DD037
P 2750 6650
F 0 "U3" H 2900 6850 60  0000 L CNN
F 1 "LM339ANSR" H 2900 6750 60  0001 L CNN
F 2 "digikey-footprints:SOIC-14_W3.9mm" H 2950 6850 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Flm139a" H 2950 6950 60  0001 L CNN
F 4 "296-6604-1-ND" H 2950 7050 60  0001 L CNN "Digi-Key_PN"
F 5 "LM339ADR" H 2950 7150 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 2950 7250 60  0001 L CNN "Category"
F 7 "Linear - Comparators" H 2950 7350 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Flm139a" H 2950 7450 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/LM339ADR/296-6604-1-ND/404682" H 2950 7550 60  0001 L CNN "DK_Detail_Page"
F 10 "IC QUAD DIFF COMPARATOR 14 -SOIC" H 2950 7650 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 2950 7750 60  0001 L CNN "Manufacturer"
F 12 "Active" H 2950 7850 60  0001 L CNN "Status"
	2    2750 6650
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J6
U 1 1 625F42AF
P 5600 1800
F 0 "J6" V 5660 1841 50  0000 L CNN
F 1 "12V_supply" V 5751 1841 50  0000 L CNN
F 2 "Connector_JST:JST_XH_B02B-XH-A_1x02_P2.50mm_Vertical" H 5600 1800 50  0001 C CNN
F 3 "~" H 5600 1800 50  0001 C CNN
	1    5600 1800
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 625F433E
P 5600 2000
F 0 "#PWR0112" H 5600 1750 50  0001 C CNN
F 1 "GND" H 5605 1827 50  0000 C CNN
F 2 "" H 5600 2000 50  0001 C CNN
F 3 "" H 5600 2000 50  0001 C CNN
	1    5600 2000
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0113
U 1 1 625F441A
P 5300 2200
F 0 "#PWR0113" H 5300 2050 50  0001 C CNN
F 1 "+12V" V 5315 2328 50  0000 L CNN
F 2 "" H 5300 2200 50  0001 C CNN
F 3 "" H 5300 2200 50  0001 C CNN
	1    5300 2200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5300 2200 5500 2200
Wire Wire Line
	5500 2200 5500 2050
$Comp
L power:+12V #PWR08
U 1 1 625F677F
P 2650 5700
F 0 "#PWR08" H 2650 5550 50  0001 C CNN
F 1 "+12V" V 2665 5828 50  0000 L CNN
F 2 "" H 2650 5700 50  0001 C CNN
F 3 "" H 2650 5700 50  0001 C CNN
	1    2650 5700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2650 5700 2750 5700
Wire Wire Line
	2750 5700 2750 5900
$Comp
L Device:C C3
U 1 1 625F79E1
P 2900 5550
F 0 "C3" V 3152 5550 50  0000 C CNN
F 1 "0.1uF" V 3061 5550 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2938 5400 50  0001 C CNN
F 3 "~" H 2900 5550 50  0001 C CNN
	1    2900 5550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2750 5700 2750 5550
Connection ~ 2750 5700
$Comp
L power:GND #PWR0114
U 1 1 625F8C86
P 3050 5550
F 0 "#PWR0114" H 3050 5300 50  0001 C CNN
F 1 "GND" H 3055 5377 50  0000 C CNN
F 2 "" H 3050 5550 50  0001 C CNN
F 3 "" H 3050 5550 50  0001 C CNN
	1    3050 5550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 625F8D2F
P 2750 6300
F 0 "#PWR0115" H 2750 6050 50  0001 C CNN
F 1 "GND" H 2755 6127 50  0000 C CNN
F 2 "" H 2750 6300 50  0001 C CNN
F 3 "" H 2750 6300 50  0001 C CNN
	1    2750 6300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 626D2EBF
P 1300 6250
F 0 "R4" V 1507 6250 50  0000 C CNN
F 1 "18kOhm" V 1416 6250 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 1230 6250 50  0001 C CNN
F 3 "~" H 1300 6250 50  0001 C CNN
	1    1300 6250
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R1
U 1 1 626D2F63
P 1300 6600
F 0 "R1" V 1507 6600 50  0000 C CNN
F 1 "10kOhm" V 1416 6600 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 1230 6600 50  0001 C CNN
F 3 "~" H 1300 6600 50  0001 C CNN
	1    1300 6600
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 626D3215
P 1150 6600
F 0 "#PWR0105" H 1150 6350 50  0001 C CNN
F 1 "GND" H 1155 6427 50  0000 C CNN
F 2 "" H 1150 6600 50  0001 C CNN
F 3 "" H 1150 6600 50  0001 C CNN
	1    1150 6600
	1    0    0    -1  
$EndComp
Text Label 950  6250 2    50   ~ 0
V_LA
Wire Wire Line
	1150 6250 950  6250
$Comp
L Device:R R5
U 1 1 626D571B
P 1400 5450
F 0 "R5" V 1607 5450 50  0000 C CNN
F 1 "10kOhm" V 1516 5450 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 1330 5450 50  0001 C CNN
F 3 "~" H 1400 5450 50  0001 C CNN
	1    1400 5450
	-1   0    0    1   
$EndComp
$Comp
L Device:R R8
U 1 1 626D57DF
P 1800 5450
F 0 "R8" V 2007 5450 50  0000 C CNN
F 1 "20kOhm" V 1916 5450 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 1730 5450 50  0001 C CNN
F 3 "~" H 1800 5450 50  0001 C CNN
	1    1800 5450
	-1   0    0    1   
$EndComp
$Comp
L power:+12V #PWR0103
U 1 1 626D595A
P 1950 5200
F 0 "#PWR0103" H 1950 5050 50  0001 C CNN
F 1 "+12V" V 1965 5328 50  0000 L CNN
F 2 "" H 1950 5200 50  0001 C CNN
F 3 "" H 1950 5200 50  0001 C CNN
	1    1950 5200
	0    1    1    0   
$EndComp
Wire Wire Line
	1950 5200 1800 5200
Wire Wire Line
	1800 5200 1800 5300
Wire Wire Line
	1800 5600 1800 5750
Wire Wire Line
	1800 5750 1600 5750
Wire Wire Line
	1400 5750 1400 5600
$Comp
L power:GND #PWR0104
U 1 1 626D7A29
P 1400 5300
F 0 "#PWR0104" H 1400 5050 50  0001 C CNN
F 1 "GND" H 1405 5127 50  0000 C CNN
F 2 "" H 1400 5300 50  0001 C CNN
F 3 "" H 1400 5300 50  0001 C CNN
	1    1400 5300
	-1   0    0    1   
$EndComp
Wire Wire Line
	2450 6000 1600 6000
Wire Wire Line
	1600 6000 1600 5750
Connection ~ 1600 5750
Wire Wire Line
	1600 5750 1400 5750
Wire Wire Line
	1450 6600 1700 6600
Wire Wire Line
	1700 6600 1700 6250
Wire Wire Line
	1700 6250 1450 6250
Wire Wire Line
	1700 6200 1700 6250
Connection ~ 1700 6250
$Comp
L Device:R R9
U 1 1 626D99D0
P 3550 5800
F 0 "R9" V 3757 5800 50  0000 C CNN
F 1 "330kOhm" V 3666 5800 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 3480 5800 50  0001 C CNN
F 3 "~" H 3550 5800 50  0001 C CNN
	1    3550 5800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3700 6100 3050 6100
$Comp
L power:+5V #PWR0108
U 1 1 626DBBB3
P 2350 7250
F 0 "#PWR0108" H 2350 7100 50  0001 C CNN
F 1 "+5V" V 2365 7378 50  0000 L CNN
F 2 "" H 2350 7250 50  0001 C CNN
F 3 "" H 2350 7250 50  0001 C CNN
	1    2350 7250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2450 7250 2350 7250
Wire Wire Line
	3700 6900 2300 6900
Wire Wire Line
	2300 6900 2300 7050
Wire Wire Line
	2300 7050 2450 7050
Wire Wire Line
	1700 6200 2250 6200
Wire Wire Line
	3700 6100 3700 6900
Wire Wire Line
	3700 5800 3700 6100
Connection ~ 3700 6100
Wire Wire Line
	3400 5800 2250 5800
Wire Wire Line
	2250 5800 2250 6200
Connection ~ 2250 6200
Wire Wire Line
	2250 6200 2450 6200
$Comp
L Device:R R10
U 1 1 626E1177
P 4250 6100
F 0 "R10" V 4457 6100 50  0000 C CNN
F 1 "10kOhm" V 4366 6100 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4180 6100 50  0001 C CNN
F 3 "~" H 4250 6100 50  0001 C CNN
	1    4250 6100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4100 6100 3700 6100
$Comp
L power:+12V #PWR01
U 1 1 626E1DC1
P 4400 6100
F 0 "#PWR01" H 4400 5950 50  0001 C CNN
F 1 "+12V" V 4415 6228 50  0000 L CNN
F 2 "" H 4400 6100 50  0001 C CNN
F 3 "" H 4400 6100 50  0001 C CNN
	1    4400 6100
	0    1    1    0   
$EndComp
$Comp
L Device:R R11
U 1 1 626E1E04
P 4300 7150
F 0 "R11" V 4507 7150 50  0000 C CNN
F 1 "10kOhm" V 4416 7150 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4230 7150 50  0001 C CNN
F 3 "~" H 4300 7150 50  0001 C CNN
	1    4300 7150
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR0109
U 1 1 626E2A8E
P 4450 7150
F 0 "#PWR0109" H 4450 7000 50  0001 C CNN
F 1 "+5V" V 4465 7278 50  0000 L CNN
F 2 "" H 4450 7150 50  0001 C CNN
F 3 "" H 4450 7150 50  0001 C CNN
	1    4450 7150
	0    1    1    0   
$EndComp
Wire Notes Line
	550  4700 5100 4700
Wire Notes Line
	5100 4700 5100 8250
Wire Notes Line
	5100 8250 550  8250
Wire Notes Line
	550  8250 550  4700
Text Notes 2400 4850 0    50   ~ 0
Comparator Circuit
$Comp
L dk_Logic-Gates-and-Inverters:SN74LVC1G04DBVR U1
U 1 1 626F1A2D
P 6600 5300
F 0 "U1" H 6100 5600 60  0000 L CNN
F 1 "Inverter" H 6100 5500 60  0000 L CNN
F 2 "digikey-footprints:SC-70-5" H 6800 5500 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g04" H 6800 5600 60  0001 L CNN
F 4 "296-11599-1-ND" H 6800 5700 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74LVC1G04DBVR" H 6800 5800 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 6800 5900 60  0001 L CNN "Category"
F 7 "Logic - Gates and Inverters" H 6800 6000 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc1g04" H 6800 6100 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74LVC1G04DBVR/296-11599-1-ND/385738" H 6800 6200 60  0001 L CNN "DK_Detail_Page"
F 10 "IC INVERTER 1CH 1-INP SOT23-5" H 6800 6300 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 6800 6400 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6800 6500 60  0001 L CNN "Status"
	1    6600 5300
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0116
U 1 1 626F209D
P 6600 5000
F 0 "#PWR0116" H 6600 4850 50  0001 C CNN
F 1 "+5V" V 6615 5128 50  0000 L CNN
F 2 "" H 6600 5000 50  0001 C CNN
F 3 "" H 6600 5000 50  0001 C CNN
	1    6600 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 5100 6600 5000
$Comp
L power:GND #PWR0117
U 1 1 626F313D
P 6500 5600
F 0 "#PWR0117" H 6500 5350 50  0001 C CNN
F 1 "GND" H 6505 5427 50  0000 C CNN
F 2 "" H 6500 5600 50  0001 C CNN
F 3 "" H 6500 5600 50  0001 C CNN
	1    6500 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 5600 6500 5500
Wire Wire Line
	3050 7150 3900 7150
Wire Wire Line
	6300 5300 5750 5300
Wire Wire Line
	5350 6750 3900 6750
Wire Wire Line
	3900 6750 3900 7150
Connection ~ 3900 7150
Wire Wire Line
	3900 7150 4150 7150
Text Label 10400 4200 2    50   ~ 0
Lithium_Mosfet_sig
Text Label 10900 3450 0    50   ~ 0
Lith_out
Text Label 10950 5350 0    50   ~ 0
LA_out
Wire Wire Line
	15000 5700 15000 5500
Wire Wire Line
	15000 5500 14600 5500
$Comp
L Device:CP C1
U 1 1 6271DC41
P 14600 6200
F 0 "C1" V 14855 6200 50  0000 C CNN
F 1 "330uF" V 14764 6200 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D10.0mm_P3.80mm" H 14638 6050 50  0001 C CNN
F 3 "~" H 14600 6200 50  0001 C CNN
	1    14600 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	14600 6050 14600 5500
$Comp
L power:GND #PWR0118
U 1 1 62724B65
P 14600 6350
F 0 "#PWR0118" H 14600 6100 50  0001 C CNN
F 1 "GND" H 14605 6177 50  0000 C CNN
F 2 "" H 14600 6350 50  0001 C CNN
F 3 "" H 14600 6350 50  0001 C CNN
	1    14600 6350
	0    1    1    0   
$EndComp
Wire Notes Line
	5300 4400 5300 6600
Text Notes 6400 4550 0    50   ~ 0
Inverter and MOSFET Driver
Text Notes 9650 3450 0    50   ~ 0
MOSFETS/Source Switching
Text Notes 12450 3450 0    50   ~ 0
LED source indicators
NoConn ~ 2450 6550
NoConn ~ 2450 6750
NoConn ~ 3050 6650
NoConn ~ 2450 7600
NoConn ~ 2450 7800
NoConn ~ 3050 7700
Wire Wire Line
	5350 5300 5350 6750
Wire Wire Line
	6800 5300 7550 5300
Wire Wire Line
	7550 5500 7250 5500
Wire Wire Line
	7250 5500 7250 6100
Wire Wire Line
	7250 6100 5750 6100
Wire Wire Line
	5750 6100 5750 5300
Connection ~ 5750 5300
Wire Wire Line
	5750 5300 5350 5300
Wire Notes Line
	5300 4400 9450 4400
Wire Notes Line
	9450 4400 9450 6600
Wire Notes Line
	9450 6600 5300 6600
Wire Wire Line
	9850 4350 9850 5300
Wire Wire Line
	9850 5300 8350 5300
Wire Wire Line
	9850 4350 10400 4350
Wire Wire Line
	10400 4200 10400 4350
Wire Wire Line
	10450 5500 10450 6500
Wire Wire Line
	8350 5500 10450 5500
Connection ~ 10450 6500
Wire Wire Line
	10150 6500 10450 6500
Wire Notes Line
	9550 3300 12050 3300
Wire Notes Line
	12050 3300 12050 7450
Wire Notes Line
	12050 7450 9550 7450
Wire Notes Line
	9550 3300 9550 7450
Wire Wire Line
	10900 4050 10900 3950
Wire Wire Line
	10900 3950 12800 3950
Wire Wire Line
	12800 3950 12800 4050
Connection ~ 10900 3950
Wire Wire Line
	10900 3950 10900 3900
Wire Wire Line
	10950 6200 10950 6050
Wire Wire Line
	12750 6300 12750 6050
Wire Wire Line
	12750 6050 10950 6050
Connection ~ 10950 6050
Wire Wire Line
	10950 6050 10950 5950
Wire Wire Line
	10900 3600 14600 3600
Wire Wire Line
	14600 3600 14600 5500
Connection ~ 14600 5500
Wire Wire Line
	10950 5650 10950 5500
Wire Wire Line
	10950 5500 14600 5500
Wire Wire Line
	10950 5350 10950 5500
Connection ~ 10950 5500
Wire Wire Line
	10900 3450 10900 3600
Connection ~ 10900 3600
Wire Notes Line
	12200 3300 13500 3300
Wire Notes Line
	13500 3300 13500 7450
Wire Notes Line
	13500 7450 12200 7450
Wire Notes Line
	12200 7450 12200 3300
Wire Notes Line
	14000 4700 15550 4700
Wire Notes Line
	15550 4700 15550 6900
Wire Notes Line
	15550 6900 14000 6900
Wire Notes Line
	14000 6900 14000 4700
Text Notes 14700 4800 0    50   ~ 0
Output Connection
Wire Notes Line
	1650 1300 7800 1300
Wire Notes Line
	7800 1300 7800 2850
Wire Notes Line
	7800 2850 1650 2850
Wire Notes Line
	1650 2850 1650 1300
Text Notes 3950 1400 0    50   ~ 0
Input Connections
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 6276D2DE
P 6450 2000
F 0 "#FLG0101" H 6450 2075 50  0001 C CNN
F 1 "PWR_FLAG" H 6450 2174 50  0000 C CNN
F 2 "" H 6450 2000 50  0001 C CNN
F 3 "~" H 6450 2000 50  0001 C CNN
	1    6450 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 2000 6450 2100
Wire Wire Line
	6450 2100 6750 2100
Connection ~ 6750 2100
Wire Wire Line
	6750 2100 6750 2250
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 627702CE
P 5200 2000
F 0 "#FLG0102" H 5200 2075 50  0001 C CNN
F 1 "PWR_FLAG" H 5200 2174 50  0000 C CNN
F 2 "" H 5200 2000 50  0001 C CNN
F 3 "~" H 5200 2000 50  0001 C CNN
	1    5200 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 2000 5200 2050
Wire Wire Line
	5200 2050 5500 2050
Connection ~ 5500 2050
Wire Wire Line
	5500 2050 5500 2000
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 627737CC
P 7550 2150
F 0 "#FLG0103" H 7550 2225 50  0001 C CNN
F 1 "PWR_FLAG" H 7550 2324 50  0000 C CNN
F 2 "" H 7550 2150 50  0001 C CNN
F 3 "~" H 7550 2150 50  0001 C CNN
	1    7550 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 2000 6850 2150
Wire Wire Line
	7550 2150 6850 2150
Connection ~ 6850 2150
Wire Wire Line
	6850 2150 6850 2250
$EndSCHEMATC
